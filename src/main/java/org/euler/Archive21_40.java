package org.euler;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.math.BigInteger;
import java.util.*;

public class Archive21_40 {
    //------------------------------------------------------------------------------------------------------------------
    static void challenge21(){
        // Evaluate the sum of all the amicable numbers (sum of divisors) under 10000.
        long sum = 0;
        int amicable1 = 0;
        int amicable2 = 0;
        int limit = 10001;

        for (int i = 220; i < limit; i++){
            // checking each numbers divisors and sum them
            for (int j = 1; j < i; j++){
                if (i % j == 0) amicable1 = amicable1 + j;
            }
            // looping above finding number to see whether it is amicable or not
            for (int z = 1; z < amicable1; z++){
                if (amicable1 % z == 0) amicable2 = amicable2 + z;
            }
            // if yes, add it to sum
            if (amicable2 == i && amicable1 != amicable2) sum = sum + amicable1;
            // reset the numbers and keep looping
            amicable1 = 0;
            amicable2 = 0;
        }
        System.out.println(sum);
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge22(){
        // What is the total of all the name scores in the file?
        String fileName = "/home/sercan/projects/projecteuler/p022_names.txt";
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        ArrayList<String> names = new ArrayList<>();
        int currentScore = 0;
        long score = 0;

        try{
            // setting file reading and storing names by skipping " and splitting by ,
            File file = new File(fileName);
            FileReader fr = new FileReader(file);
            LineNumberReader reader = new LineNumberReader(fr);
            StringBuilder res = new StringBuilder();

            int character;
            while ((character = reader.read()) != -1){
                char c = (char) character;
                if (c == ','){ // comma means that name is ready to be added. there is no comma at the end, add last name after loop
                    names.add(res.toString());
                    res.setLength(0);
                }else if (c == '\"'){
                    continue; // skip all double quotes
                }else{
                    res.append(Character.toLowerCase(c)); // add the char as a part of name
                }
            }

            // last name should be considered since there is no comma at the end
            names.add(res.toString());

        }catch (IOException e){
            e.printStackTrace();
        }

        // sorting names
        Collections.sort(names);

        // calculating scores
        for (int i = 0; i < names.size(); i++){
            for (int j = 0; j < names.get(i).length(); j++){
                for (int z = 0; z < alphabet.length(); z++){
                    if (names.get(i).charAt(j) == alphabet.charAt(z)){
                        currentScore = currentScore + (z + 1);
                        break;
                    }
                }
            }
            score = score + currentScore * (i + 1);
            currentScore = 0;
        }
        System.out.println(score);
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge23(){
        // Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers. (limit is 28123)
        int limit = 28123;
        long sum = 0;
        long sumAbundant = 0;
        long divisorSum = 0;
        ArrayList<Integer> abundant = new ArrayList<>();
        ArrayList<Integer> abundantSum = new ArrayList<>();

        // finding abundant numbers under 28123
        for (int i = 1; i < limit + 1; i++){
            for (int j = 1; j < i; j++){
                if (i % j == 0) divisorSum = divisorSum + j;
            }
            if (divisorSum > i) abundant.add(i);
            divisorSum = 0;
            sum = sum + i; // finding sum of all the numbers under limit
        }

        // finding all the numbers which cannot be written as the sum of two abundant numbers based on above solution
        int number;
        for (int i = 0; i < abundant.size(); i++) {
            for (int j = i; j < abundant.size(); j++){
                number = abundant.get(i) + abundant.get(j);
                if (number <= limit && !abundantSum.contains(number)) {
                    abundantSum.add(number);
                    sumAbundant = sumAbundant + number;
                }
            }
        }
        System.out.println(sum - sumAbundant);
    }

    //------------------------------------------------------------------------------------------------------------------
    // not my solution
    static void challenge24(){
        // What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
        // Initialize
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++){
            array[i] = i;
        }

        // Permute
        for (int i = 0; i < 999999; i++) {
            if (!nextPermutation(array)) throw new AssertionError();
            System.out.println(Arrays.toString(array));
        }

        // Format output
        StringBuilder ans = new StringBuilder();
        for (int value : array) ans.append(value);
        System.out.println(ans);
    }

    static boolean nextPermutation(int[] array) {
        // Find longest non-increasing suffix
        int i = array.length - 1;
        while (i > 0 && array[i - 1] >= array[i])
            i--;
        // Now i is the head index of the suffix

        // Are we at the last permutation already?
        if (i <= 0)
            return false;

        // Let array[i - 1] be the pivot
        // Find rightmost element that exceeds the pivot
        int j = array.length - 1;
        while (array[j] <= array[i - 1])
            j--;
        // Now the value array[j] will become the new pivot
        // Assertion: j >= i

        // Swap the pivot with j
        int temp = array[i - 1];
        array[i - 1] = array[j];
        array[j] = temp;

        // Reverse the suffix
        j = array.length - 1;
        while (i < j) {
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
            i++;
            j--;
        }

        // Successfully computed the next permutation
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge25(){
        // What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
        BigInteger x = BigInteger.ZERO;
        BigInteger y = BigInteger.ONE;
        BigInteger z = x.add(y);
        long count = 2;

        while (z.toString().length() < 1000){
            x = y;
            y = z;
            z = x.add(y);
            count++;
        }
        System.out.println(count);
    }

    //------------------------------------------------------------------------------------------------------------------
    // not my solution
    static void challenge26(){
        // Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
        int bestNumber = 0;
        int bestLength = 0;
        for (int i = 1; i <= 1000; i++) {
            int len = getCycleLength(i);
            if (len > bestLength) {
                bestNumber = i;
                bestLength = len;
            }
        }
        System.out.println(bestNumber);
    }

    private static int getCycleLength(int n) {
        Map<Integer,Integer> stateToIter = new HashMap<>();
        int state = 1;
        int iter = 0;
        while (!stateToIter.containsKey(state)) {
            stateToIter.put(state, iter);
            state = state * 10 % n;
            iter++;
        }
        return iter - stateToIter.get(state);
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge27(){
        // Quadratic primes: n^2+an+b , where |a|<1000 and |b|≤1000. Find the product of the coefficients, a and b
        int max = 0;
        int product = 0;
        int n;
        int result;

        for (int a = -999; a < 1000; a++){ // first coefficient loop
            for (int b = -1000; b < 1001; b++){ // second coefficient loop

                // calculate the result of equation. then check if it is prime or not
                n = 0; // n starts from zero
                while (true){
                    result = n * n + a * n + b;
                    if (!isPrime(result)) break;
                    n++;
                }
                if (max < n){
                    max = n;
                    product = a * b;
                }
            }
        }
        System.out.println(product + " " + max);
    }

    static boolean isPrime(int number){
        if (number <= 1) return false;

        for (int i = 2; i < number; i++){
            if (number % i == 0){
                return false;
            }
        }
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge28(){
        // What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed?
        // math model: on each circle diagonal values are increasing by 2, 4, 6, 8 ....
        int dim = 1001;
        int mid = dim / 2;
        int number = 1;
        int increment = 2;
        int sum = 0;

        sum += number;
        for (int i = 0; i < mid; i++){
            for (int j = 0; j < 4; j++){
                number += increment;
                sum = sum + number;
            }
            increment += 2;
        }
        System.out.println(sum);
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge29(){
        // How many distinct terms are in the sequence generated by a^b for 2 ≤ a ≤ 100 and 2 ≤ b ≤ 100?
        ArrayList<BigInteger> terms = new ArrayList<>();
        BigInteger number;

        for (int a = 2; a < 101; a++){
            for (int b = 2; b < 101; b++){
                number = new BigInteger(String.valueOf(a)).pow(b);
                if (!terms.contains(number)) terms.add(number);
            }
        }
        System.out.println(terms.size());
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge30(){
        // Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
        long result = 0;
        long sum = 0;
        String num;

        // the highest digit is 9 and 9^5=59049, which has five digits. This implies that the highest possible number
        // which equals the sum of the fifth power of its digits is 5 times 9^5=295245.
        int max = (int) (Math.pow(9, 5) * 5);

        for (long i = 2; i < max; i++){
            num = String.valueOf(i);
            for (int j = 0; j < num.length(); j++){
                sum = (long) (sum + Math.pow(Character.getNumericValue(num.charAt(j)), 5));
            }
            if (sum == i) result = result + sum;
            sum = 0;
        }
        System.out.println(result);
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge31(){
        // How many different ways can £2 (200) be made using any number of coins (1, 2, 5, 10, 20, 50, 100, 200)?
        /*
         * 0    | 1 2 3 4 5 6 7 8 9 10 11 ............. 200 first element of this array will be ignored. that column represents coin types. (as in challenge-15)
         * ------------------------------------------------
         * 1    | this row shows possibilities with only 1
         * 2    | this row shows possibilities with 1 and 2
         * 5    | this row shows possibilities with 1, 2 and 5
         * 10   | .
         * 20   | .
         * 50   | .
         * 100  | .
         * 200  | this row shows possibilities with all coins
         */
        int[] coins = {1, 2, 5, 10, 20, 50, 100, 200};
        int m = coins.length;
        int n = 200;
        int[] result = new int[n+1];

        result[0] = 1;

        for(int i = 0; i < m; i++){
            for(int j = coins[i]; j <= n; j++){
                result[j] += result[j - coins[i]];
            }
            System.out.println(Arrays.toString(result));
        }
        System.out.println(result[n]);
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge32(){
        // Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
        int product;
        int digit;
        ArrayList<Integer> productList = new ArrayList<>();
        ArrayList<Integer> digits = new ArrayList<>();
        int sum = 0;

        // multiplicand or multiplier cannot be 5 digits, because in this case product will be 5 digits as well, but we need in total 9 digits
        // so limit is biggest 4 digit value
        for (int i = 1; i < 10000; i++){
            for (int j = 1; j < 10000; j++){
                product = i * j; // calculating product
                // if total length of i, j and product is 9, then add each char in them to digits ArrayList and control that we have 123456789 or not
                if (String.valueOf(i).length() + String.valueOf(j).length() + String.valueOf(product).length() == 9){
                    for (int k = 0; k < String.valueOf(i).length(); k++){
                        digit = Character.getNumericValue(String.valueOf(i).charAt(k));
                        if (!digits.contains(digit) && digit != 0){
                            digits.add(digit);
                        }
                    }
                    for (int k = 0; k < String.valueOf(j).length(); k++){
                        digit = Character.getNumericValue(String.valueOf(j).charAt(k));
                        if (!digits.contains(digit) && digit != 0){
                            digits.add(digit);
                        }
                    }
                    for (int k = 0; k < String.valueOf(product).length(); k++){
                        digit = Character.getNumericValue(String.valueOf(product).charAt(k));
                        if (!digits.contains(digit) && digit != 0){
                            digits.add(digit);
                        }
                    }
                    if (digits.size() == 9 && !productList.contains(product)) productList.add(product);
                    digits.clear();
                }
            }
        }
        for (Integer integer : productList) {
            sum = sum + integer;
        }
        System.out.println(sum);
        /*
            Another solution is to join string values of i, j and product, and then sort the array.
            After that check if it is equal to "123456789" or not. see below alternative solution
        */
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge32_alternative(){
        // Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
        int product;
        int sum = 0;
        ArrayList<Integer> productList = new ArrayList<>();
        String result;

        // multiplicand or multiplier cannot be 5 digits, because in this case product will be 5 digits as well, but we need in total 9 digits
        // so limit is biggest 4 digit value
        for (int i = 1; i < 10000; i++){
            for (int j = 1; j < 10000; j++){
                product = i * j; // calculating product
                // if total length of i, j and product is 9 then join them, sort it and check we have 123456789 or not
                if (String.valueOf(i).length() + String.valueOf(j).length() + String.valueOf(product).length() == 9){
                    result = String.join(String.valueOf(i), String.valueOf(j), String.valueOf(product));
                    char[] sortedResult = result.toCharArray();
                    Arrays.sort(sortedResult);
                    if (String.valueOf(sortedResult).equals("123456789") && !productList.contains(product)) productList.add(product);
                }
            }
        }
        for (Integer integer : productList) {
            sum = sum + integer;
        }
        System.out.println(sum);
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge33(){
        // 49/98 = 4/8, which is correct, is obtained by cancelling the 9s. There are exactly four non-trivial
        // examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.
        // If the product of these four fractions is given in its lowest common terms, find the value of the denominator.
        float above;
        float below;
        int cancelAbove;
        int cancelBelow;
        float product = 1;

        for (float i = 1; i < 100; i++){
            for (float j = i + 1; j < 100; j++){
                above = Character.getNumericValue(String.valueOf(i).charAt(0));
                below = Character.getNumericValue(String.valueOf(j).charAt(1));
                cancelAbove = Character.getNumericValue(String.valueOf(i).charAt(1));
                cancelBelow = Character.getNumericValue(String.valueOf(j).charAt(0));
                if (below != 0 && (i / j) == (above / below) && cancelAbove == cancelBelow){
                    System.out.println("above: " + i + "\tbelow: " + j);
                    product = product * below / above;
                }
            }
        }
        System.out.println(product);
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge34(){
        // Find the sum of all numbers which are equal to the sum of the factorial of their digits.
        // 9! = 362880, both seven and eight digits of 9 will give the result of 7 digits. So upper bound will be 7*9!
        // there might be some optimization based on above logic, for example any number with digit 9 cannot be less than 9!, so you can skip them
        long result = 0;
        long sum = 0;
        String digit;

        for (int i = 3; i < 2903041; i++){
            digit = String.valueOf(i);
            for (int j = 0; j < digit.length(); j++){
                sum = sum + tailRecFact(Character.getNumericValue(digit.charAt(j)), 1);
            }
            if (sum == i) result = result + i;
            sum = 0;
        }
        System.out.println(result);
    }

    // tail recursion for factorial
    static long tailRecFact(int number1, int number2){
        if (number1 == 0) return number2;

        return tailRecFact(number1 - 1, number2 * number1);
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge35(){
        // How many circular primes are there below one million?
        // The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
        ArrayList<Integer> circularPrimes = new ArrayList<>();
        int limit = 1000000;
        int rotated;

        for (int i = 2; i < limit; i++){
            if (isPrime(i)){ // if the number is prime, start rotation one by one and check rotated ones are also prime or not
                rotated = i;
                for (int j = 0; j < String.valueOf(i).length(); j++){
                    // build-in function Integer.rotateLeft() can be used as well
                    rotated = rotate(rotated);
                    if (!isPrime(rotated)) break;
                }
                // if all rotated one prime, after full turn, we should find same number
                if (rotated == i) circularPrimes.add(i);
            }
        }
        System.out.println(circularPrimes.size());
    }

    // function to rotate a number
    static int rotate(int number){
        char initial = String.valueOf(number).charAt(0);
        String strNumber = String.valueOf(number);
        String[] arrayNumber = new String[strNumber.length()];
        StringBuilder result = new StringBuilder();

        for (int i = 1; i < strNumber.length(); i++){
            arrayNumber[i-1] = String.valueOf(strNumber.charAt(i));
        }
        arrayNumber[strNumber.length()-1] = String.valueOf(initial);

        for (String each : arrayNumber){
            result.append(each);
        }
        return Integer.parseInt(String.valueOf(result));
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge36(){
        // Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
        long sum = 0;
        int limit = 1000000;

        for (int i = 1; i < limit; i++){
            if (isPalindrome(String.valueOf(i)) && isPalindrome(Integer.toBinaryString(i))) sum = sum + i;
        }
        System.out.println(sum);
    }

    static boolean isPalindrome(String value){
        int size = value.length();
        StringBuilder reverse = new StringBuilder();

        for (int i = size - 1; i > -1; i--){
            reverse.append(value.charAt(i));
        }
        return reverse.toString().equals(value);
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge37(){
        // Find the sum of the only eleven primes that are both truncatable from left to right and right to left.
        ArrayList<Integer> truncatable = new ArrayList<>();
        String strNumber;
        String newNumber;
        String lastDigit;
        String firstDigit;
        int number = 22;
        boolean flag;
        int sum = 0;

        while (truncatable.size() < 11){
            // converting number to string and check the last/first chars
            strNumber = String.valueOf(number);
            lastDigit = Character.toString(strNumber.charAt(strNumber.length() - 1));
            firstDigit = Character.toString(strNumber.charAt(0));
            flag = true;

            // if last and first chars are 2, 3, 5 or 7, then check if it is truncatable or not
            if ((lastDigit.equals("2") ||  lastDigit.equals("3") || lastDigit.equals("5")|| lastDigit.equals("7"))
                    && (firstDigit.equals("2") || firstDigit.equals("3") || firstDigit.equals("5") || firstDigit.equals("7")) && isPrime(number)){
                // if last and first chars are desired numbers, and number is prime, then create a string builder and start removing chars from left side
                StringBuilder sbNumber = new StringBuilder(strNumber);
                for (int i = 0; i < strNumber.length() - 1; i++){
                    newNumber = sbNumber.deleteCharAt(0).toString();
                    if (!isPrime(Integer.parseInt(newNumber))){
                        flag = false;
                        break;
                    }
                }

                // after first loop, (checking from left to right), if condition is met, check reverse
                sbNumber = new StringBuilder(strNumber);
                if (flag){
                    for (int i = 0; i < strNumber.length() - 1; i++){
                        newNumber = sbNumber.deleteCharAt(sbNumber.length() - 1).toString();
                        if (!isPrime(Integer.parseInt(newNumber))){
                            flag = false;
                            break;
                        }
                    }
                }

                // if slag is still true, then number is truncatable
                if (flag){
                    truncatable.add(number);
                    sum = sum + number;
                }
            }
            number++;
        }
        System.out.println(sum);
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge38(){
        // What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?
        // n is 2. so number should be between 9876 and 5000, because when any number in this range is multiplied with 2 gives 5 digits result
        // n is chosen as 2 to set first half of number as bigger as possible
        StringBuilder number = new StringBuilder();
        String strNumber = "";
        int i = 9876;

        while (i > 5000){
            number.append(i);
            number.append(i * 2);
            strNumber = number.toString();
            if (isAllDigits(strNumber)) break;
            i--;
            number.setLength(0);
        }
        System.out.println(strNumber);
    }

    // all digits check
    static boolean isAllDigits (String number){
        ArrayList<Integer> digits = new ArrayList<>();
        int digit;

        for (int i = 0; i < number.length(); i++){
            digit = Character.getNumericValue(number.charAt(i));
            if (!digits.contains(digit) && digit != 0) digits.add(digit);
        }
        return digits.size() == 9;
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge39(){
        // If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly three solutions for p = 120.
        //{20,48,52}, {24,45,51}, {30,40,50}
        // For which value of p ≤ 1000, is the number of solutions maximised?

        /*
            Conditions:
                a^2 + b^2 = c^2 (1)
                a + b + c = p (2)
            Thus we can rewrite (2) as c = p  – a – b and insert it into (1) yielding
                a^2 + b^2 = (p-a-b)^2 = p^2 + a^2 + b^2 -2pa – 2pb + 2ab
            Isolating b on one side of that equation yields
                b = (p^2 -2pa) / (2p-2a)
        */

        long result = 0;
        long resultSolutions = 0;

        // looping p
        for (long p = 1; p <= 1000; p++){
            int numberOfSolutions = 0;

            // looping above found equation
            for (long a = 1; a < (p / 3); a++){
                if (p * (p - 2 * a) % (2 * (p - a)) == 0) numberOfSolutions++;
            }

            // if new result is higher, then change existing one
            if(numberOfSolutions > resultSolutions){
                resultSolutions = numberOfSolutions;
                result = p;
            }
        }
        System.out.println(result);
    }
    //------------------------------------------------------------------------------------------------------------------
    static void challenge40(){
        // An irrational decimal fraction is created by concatenating the positive integers:
        // 0.123456789101112131415161718192021...
        // If dn represents the nth digit of the fractional part, find the value of the following expression.
        // d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000

        // first index is zero, but above number starts with index 1 after point, so i will start with number zero to align the indexes

        StringBuilder decimal = new StringBuilder();
        String strNumber;
        int number = 0;

        while (decimal.length() < 1000001){
            decimal.append(number);
            number++;
        }
        strNumber = decimal.toString();
        System.out.println(Character.getNumericValue(strNumber.charAt(1)) * Character.getNumericValue(strNumber.charAt(10)) *
                Character.getNumericValue(strNumber.charAt(100)) * Character.getNumericValue(strNumber.charAt(1000)) *
                Character.getNumericValue(strNumber.charAt(10000)) * Character.getNumericValue(strNumber.charAt(100000)) *
                Character.getNumericValue(strNumber.charAt(1000000)));
    }
}
