package org.euler;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

public class Archive41_60 {
    //------------------------------------------------------------------------------------------------------------------
    static void challenge41(){
        // What is the largest n-digit pandigital prime that exists?

        /*
                  Pandigital          divisible by 3 or not
                1+2+3+4 = 10               -
                1+2+3+4+5 = 15             3
                1+2+3+4+5+6 = 21           3
                1+2+3+4+5+6+7 = 28         -
                1+2+3+4+5+6+7+8 = 36       3
                1+2+3+4+5+6+7+8+9 = 45     3
             so upper limit will be the one with 7 digits, 7654321
        */

        long i = 7654321;

        while (i > 2) {
            if (isPandigital(String.valueOf(i))){
                if (isPrime(i)) break;
            }
            i -= 2; // skipping even numbers
        }
        System.out.println(i);
    }

    // check a number is prime or not function
    static boolean isPrime(long number){
        if (number <= 1) return false;

        // to perform faster, I use (i * i) to check number is prime or not
        for (int i = 2; i * i < number; i++){
            if (number % i == 0){
                return false;
            }
        }
        return true;
    }

    // checking if a number includes all the digits up to its length
    static boolean isPandigital (String number){
        ArrayList<Integer> digits = new ArrayList<>();
        int n = number.length();
        int digit;

        for (int i = 0; i < n; i++){
            digit = Character.getNumericValue(number.charAt(i));
            if (!digits.contains(digit) && digit != 0 && digit <= n) digits.add(digit);
        }
        return digits.size() == n;
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge42(){
        // how many words in p042_words.txt file are triangle words? "n(n+1)/2"
        String fileName = "/home/stekin/master/projects/projecteuler/p042_words.txt";
        ArrayList<Integer> wordsValues = new ArrayList<>();
        ArrayList<Integer> triangleNumbers = new ArrayList<>();
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int wordValue = 0;
        int maxLength = 0;
        int limit;

        try{
            // setting file reading and storing names by skipping " and splitting by ,
            File file = new File(fileName);
            FileReader fr = new FileReader(file);
            LineNumberReader reader = new LineNumberReader(fr);
            StringBuilder res = new StringBuilder();

            // reading the file, recognize words, find the size of longest word to establish upper limit of triangle number,
            // calculate values of words and add them to array list
            int character;
            while ((character = reader.read()) != -1){
                char c = (char) character;
                if (c == ','){
                    if (maxLength < res.toString().length()) maxLength = res.toString().length();
                    wordsValues.add(wordValue);
                    wordValue = 0;
                    res.setLength(0);
                }else if (c == '\"') {
                    continue;
                } else{
                    res.append(Character.toLowerCase(c));
                    wordValue = wordValue + alphabet.indexOf(Character.toLowerCase(c)) + 1;
                }
            }

            // maxLength * 27 = limit of triangle numbers
            limit = 27 * maxLength;

            // creating triangle numbers array list
            int number = 1;
            int result = 1;
            while (result < limit){
                result = (int) (number * (number + 1) * 0.5);
                triangleNumbers.add(result);
                number++;
            }

            // checking how many words in triangle numbers list
            int howMany = 0;
            for (int value : wordsValues){
                if (triangleNumbers.contains(value)) howMany++;
            }

            // printing the result
            System.out.println(howMany);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge43(){
        // Find the sum of all 0 to 9 pandigital numbers with property of Sub-string divisibility
        // by using below logic, more arrays can be created and code can run faster. but this is enough to pass euler-project criteria

        // below array has numbers below 1000, can be divided by 17 and first two digits are same with last two digits of numbers below 1000 can be divided by 13
        String[] div17 = {"017","034","051","068","085","102","136","153","170","204","238","289","306","340",
                "374","391","425","459","476","493","510","527","561","629","680","697","714","731","765",
                "782","816","850","867","901","918","935","952","986"};
        long start = 1023456; // min pandigital value without its last 3 digits
        long finish = 9876543; // max pandigital value without its last 3 digits
        long sum = 0;

        while (start < finish){
            for (String s : div17) {
                if (isPandigitalIncZero(start + s) && subStringDiv(start + s)) {
                    sum += Long.parseLong(start + s);
                }
            }
            start++;
        }
        System.out.println(sum);
    }

    static boolean isPandigitalIncZero (String number){
        ArrayList<Integer> digits = new ArrayList<>();
        int n = number.length();
        int digit;

        for (int i = 0; i < n; i++){
            digit = Character.getNumericValue(number.charAt(i));
            if (!digits.contains(digit) && digit <= n) digits.add(digit);
        }
        return digits.size() == n;
    }

    static boolean subStringDiv(String number){
        int[] divisors = {2, 3, 5, 7, 11, 13};
        StringBuilder subString;

        for (int i = 1; i < 7; i++){
            subString = new StringBuilder();
            subString.append(number.charAt(i)).append(number.charAt(i + 1)).append(number.charAt(i + 2));
            if (Integer.parseInt(subString.toString()) % divisors[i - 1] != 0) return false;
        }
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge44(){
        // Find the pair of pentagonal numbers, Pj and Pk, for which their sum and difference are pentagonal
        // and D = |Pk − Pj| is minimised; what is the value of D?
        int iterator = 2;
        int a, b;
        while (true){
            a = iterator * (3 * iterator -1) / 2;
            for (int i = 1; i < iterator; i++){
                b = i * (3 * i -1) / 2;
                if (isPentagonal(a + b) && isPentagonal(a - b)){
                    System.out.println(a - b);
                    return;
                }
            }
            iterator++;
        }
    }

    static boolean isPentagonal(int number){
        // formula is from wikipedia, to check whether number is pentagonal or not
        return (1 + Math.pow((24 * number + 1), 0.5)) % 6 == 0;
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge45(){
        // Find the next triangle (T285 = P165 = H143) number that is also pentagonal and hexagonal.
        long t;
        long i = 286;
        while (true){
            t = triangle(i);
            if (isPentagonal(t) && isHexagonal(t)) break;
            i++;
        }
        System.out.println(t);
    }

    static boolean isPentagonal(long number){
        // formula is from wikipedia, to check whether number is pentagonal or not
        return (1 + Math.pow((24 * number + 1), 0.5)) % 6 == 0;
    }

    static boolean isHexagonal(long number){
        // formula is from wikipedia, to check whether number is hexagonal or not
        return ((Math.pow(8 * number + 1, 0.5)) + 1) % 4 == 0;
    }

    static long triangle(long number){
        return number * (number + 1) / 2;
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge46(){
        // What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?
        int number = 3;
        boolean flag = false;

        while (true){
            if (isOddComposite(number)){
                for (int i = 1; i < number; i++){
                    if (isPrime(i) && Math.pow((number - i) / 2.0, 0.5) == (int)Math.pow((number - i) / 2.0, 0.5)){
                        flag = false;
                        break;
                    }
                    flag = true;
                }
            }
            if (flag) break;
            number += 2;
        }
        System.out.println(number);
    }

    static boolean isOddComposite(long number){
        if (number <= 1) return false;

        for (int i = 3; i < number; i += 2){
            if (number % i == 0){
                return true;
            }
        }
        return false;
    }

    static boolean isPrime(int number){
        if (number <= 1) return false;

        for (int i = 2; i < number; i++){
            if (number % i == 0){
                return false;
            }
        }
        return true;
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge47(){
        // Find the first four consecutive integers to have four distinct prime factors each. What is the first of these numbers?
        long start = 2 * 3 * 5 * 7;
        long count = 0;

        while (count < 4){
            if (primeFactors(start) >= 4){
                count++;
            }else{
                count = 0;
            }
            start++;
        }
        System.out.println(start - 4);
    }

    static int primeFactors(long number){
        long index;
        ArrayList<Long> primeFac = new ArrayList<>();

        for (index = 2; index < number; ++index){
            while (number % index == 0){
                if (!primeFac.contains(index)) primeFac.add(index);
                number = number / index;
            }
        }
        if (index <= number && !primeFac.contains(index)) primeFac.add(index);
        return primeFac.size();
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge48(){
        // Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000
        BigInteger result = BigInteger.ZERO;

        for (int i = 1; i < 1001; i++){
            result = result.add(bigIntegerPow(i));
        }
        System.out.println(result.toString().substring(result.toString().length() - 10));
    }

    static BigInteger bigIntegerPow(int number){
        BigInteger res = BigInteger.ONE;

        for (int i = 0; i < number; i++){
            res = res.multiply(BigInteger.valueOf(number));
        }
        return res;
    }

    //------------------------------------------------------------------------------------------------------------------
    static void challenge49(){
        int number = 1488;

        while (!((number + 2 * 3330) > 9999)){
            if (isPrime(number) && isPrime(number + 3330) && isPrime(number + 2 * 3330)){
                if (sortOfDigits(number) == sortOfDigits(number + 3330) && sortOfDigits(number) == sortOfDigits(number + 2 * 3330)){
                    System.out.println(number + String.valueOf(number + 3330) + (number + 2 * 3330));
                    return;
                }
            }
            number++;
        }
    }

    static int sortOfDigits(int number){
        String strNum = String.valueOf(number);
        char[] chars = strNum.toCharArray();
        Arrays.sort(chars);
        String sorted = new String(chars);
        return Integer.parseInt(sorted);
    }

    //------------------------------------------------------------------------------------------------------------------

}
